--[[    Plugin name: Cosnt Name Cleaner
    Alias folder: None
    Desc: 清理過長常量名稱
    Author: Sam
--]]
---@class CosntNameCleaner
local this = { logic_reslove = false }

local jumpPointer, loopBlock, logicBlock, fixJumpPos

function this:init()
    log = require("util.Log")("INFO")
    jumpPointer = require("plugin.decompile.static.block.JumpPointer")
    loopBlock = require("plugin.decompile.static.block.LoopBlock")
    logicBlock = require("plugin.decompile.static.block.LogicBlockv2")
    fixJumpPos = require("plugin.decompile.static.block.FixJumps")
    return {
        id = "CosntNameCleaner",
        name = "常量名字清理",
        desc = "清理過長常量名稱",
        author = "Sam",
        config = false,
        config_settings = {}
    }
end

local logicOPs = {
    LOADBOOL = true,
    EQ = true,
    LT = true,
    LE = true,
    TEST = true,
    TESTSET = true;
}
local jumpOPs = {
    JMP = true,
    FORPREP = true,
    FORLOOP = true,
    TFORLOOP = true;
}

function this:main()
    local currLevel = Editor.currentLevel
    local info = Editor.protoLevel[currLevel]
    if info.State == Editor.States.DELETED or info.State == Editor.States.EMPTY then
        Toast(Language.err.ListIsEmpty)
        return
    end
    while true do
        local input = Choice({
            "還原邏輯指令: " ..
            Language.other[this.logic_reslove == true and "Yes" or "No"][1],
            "開始還原"
        })
        if not input or input == 0 then
            return
        elseif input == 1 then
            this.logic_reslove = not this.logic_reslove
        elseif input == 2 then
            local limit = 150
            local count = #Editor.protoLevel
            for i = 1, count do
                local count2 = #Editor.cacheProto[i].const
                for i2 = 1, count2 do
                    if Editor.cacheProto[i].const[i2].type == "string" and #Editor.cacheProto[i].const[i2][1] > limit then
                        Editor.cacheProto[i].const[i2][1] = ""
                    end
                end
            end
        end
    end
    return "close_plugin_UI"
end

return this