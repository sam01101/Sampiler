local endian;
local BinConstantType = {
    ["nil"] = "\0",
    boolean = "\1",
    number = "\3",
    string = "\4"
};

local bit32 = bit32 or require("bit32");
local EXT = bit32.extract;

local string = string;
local char = string.char
local reverse = string.reverse

local math = math;
local math_frexp = math.frexp
local math_floor = math.floor
local math_ldexp = math.ldexp

local write;

local tostring = tostring;

-- Required

local function grab_byte(v) return math_floor(v / 256),
                                   char(math_floor(v) % 256) end

local numtohex = function(number) -- turn number back to hex
    local instr = tostring(number)
    local bytes = {}
    for k = 1, 4 do bytes[k] = EXT(instr, 8 * (k - 1), 8) end
    if endian then
        bytes[4], bytes[3], bytes[2], bytes[1] = bytes[1], bytes[2], bytes[3],
                                                 bytes[4]
    end
    return char(bytes[1], bytes[2], bytes[3], bytes[4])
end

local lua_NUMBER = function(x)
    local sign = 0
    if x < 0 then
        sign = 1;
        x = -x
    end
    local mantissa, exponent = math_frexp(x)
    if x == 0 then -- zero
        mantissa, exponent = 0, 0
    elseif x ~= 0 then
        mantissa = (mantissa * 2 - 1) * math_ldexp(0.5, 53)
        exponent = exponent + 1022
    end
    local v, byte = "" -- convert to bytes
    x = mantissa
    for i = 1, 6 do
        x, byte = grab_byte(x);
        v = v .. byte
    end
    x, byte = grab_byte(exponent * 16 + x);
    v = v .. byte
    x, byte = grab_byte(sign * 128 + x);
    v = v .. byte
    if endian then return reverse(v) end
    return v
end

local setFunction = function(name, s)
    return function(...) return s[name](s, ...) end
end

-- io_write = setFunction("write", steam)

-- Main

local writePrototype;
local writeInstructions;
local writeConstants;
local writeFunctions;
local writeUpvalues;
local writeDebug;

writePrototype = function(tree)
    write(numtohex(tree.linedefined), numtohex(tree.lastlinedefined))
    write(char(tree.param), char(tree.vararg), char(tree.reg))
    writeInstructions(tree.instr)
    writeConstants(tree.const)
    writeFunctions(tree.func)
    writeUpvalues(tree.upvalue)
    writeDebug(tree.extra)
end

writeInstructions = function(instr)
    local n1 = 0
    local instrlen = #instr
    for i in ipairs(instr) do n1 = n1 + 1 end
    write(numtohex(--[[instrlen]]n1))
    for i in ipairs(instr) do write(numtohex(Bytecode:toBinary(instr[i]))) end
end

writeConstants = function(constant)
    local constlen = #constant;
    local str;
    local types;
    write(numtohex(constlen))
    for i = 1, constlen do
        types = constant[i].type;
        write(BinConstantType[types])
        if types == "nil" then
            -- write(BinConstantType.NIL)
        elseif types == "boolean" then
            write(char(constant[i][1] and 1 or 0))
        elseif types == "number" then
            write(lua_NUMBER(constant[i][1]))
        elseif types == "string" then
            str = constant[i][1] .. "\0" -- load("return '" .. constant[i][1] .. "\0'")();
            write(numtohex(#str), str);
        elseif types ~= "nil" and types ~= "boolean" and types ~= "number" and types ~= "string" then
            error("Unknown const type " .. (types or "null"))
        end
    end
end

writeFunctions = function(func)
    local funclen = #func
    write(numtohex(funclen))
    for i = 1, funclen do writePrototype(func[i]) end
end

writeUpvalues = function(upval)
    local upvaluelen = #upval
    local curr;
    write(numtohex(upvaluelen))
    for i = 1, upvaluelen do
        curr = upval[i]
        write(char(curr.instack and 1 or 0), char(curr.position))
    end
end

writeDebug = function(debug)
    --for i = 1, 16 do write(char(0)) end
    if debug.source and #debug.source > 0 then
       write(numtohex(#debug.source+1))
       write(debug.source.."\0")
    elseif not debug.source or #debug.source <= 0 then
       write(char(0):rep(4))
    end
    if debug.linenumber and #debug.linenumber > 0 then
        write(numtohex(#debug.linenumber))
        for i = 1, #debug.linenumber do
            write(numtohex(debug.linenumber[i]))
        end
    elseif not debug.linenumber or #debug.linenumber <= 0 then
        write(char(0):rep(4))
    end
    if debug.locals and #debug.locals > 0 then
        write(numtohex(#debug.locals))
        for i = 1, #debug.locals do
            local curr_loc = debug.locals[i]
            write(numtohex(#curr_loc.name+1), curr_loc.name.."\0", numtohex(curr_loc.startpc), numtohex(curr_loc.endpc))
        end
    elseif not debug.locals or #debug.locals <= 0 then
        write(char(0):rep(4))
    end
    if debug.upval and #debug.upval > 0 then
        write(numtohex(#debug.upval))
        for i = 1, #debug.upval do
            local curr_upval = debug.upval[i]
            write(numtohex(#curr_upval+1), curr_upval.."\0")
        end
    elseif not debug.upval or #debug.upval <= 0 then
        write(char(0):rep(4))
    end
end

local function main(tree, output)
    Bytecode:init("GG52")
    write = setFunction("write", output);
    local close = setFunction("close", output);
    local head = tree.header
    endian = false;
    local float = 0
    write('\027Lua\x52', char(head.RIO), char(endian and 0 or 1), char(head.int),
          char(head.instr), char(head.sizet), char(head.numlen),
          char(0), head.tail);
    writePrototype(tree.main)
end

return main
