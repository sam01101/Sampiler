--[[    Plugin name: Garbage Cleaner
    Alias folder: None
    Desc: 清理無用代碼
    Author: Sam
--]]
---@class GarbageCleaner
local this = { global = false }

local jumpPointer, loopBlock, logicBlock, fixJumpPos

function this:init()
    log = require("util.Log")("INFO")
    jumpPointer = require("plugin.decompile.static.block.JumpPointer")
    loopBlock = require("plugin.decompile.static.block.LoopBlock")
    logicBlock = require("plugin.decompile.static.block.LogicBlockv2")
    fixJumpPos = require("plugin.decompile.static.block.FixJumps")
    return {
        id = "GarbageCleaner",
        name = "垃圾清理",
        desc = "清理無用代碼",
        author = "Sam",
        config = false,
        config_settings = {}
    }
end

local logicOPs = {
    LOADBOOL = true,
    EQ = true,
    LT = true,
    LE = true,
    TEST = true,
    TESTSET = true;
}
local jumpOPs = {
    JMP = true,
    FORPREP = true,
    FORLOOP = true,
    TFORLOOP = true;
}

local ensureUsed = function(instr)
    local count = #instr
    local curr = 1
    local used = {}
    local logic = {}
    local loop = {}
    local nLoop = {} -- nagative loop
    local use = function(line)
        -- confirm used
        used[#used+1] = line
    end
    local checkUsed = function(line)
        -- check is it used or not
        for i = 1, #used do
            if used[i] ~= nil then
                return true
            end
        end
    end
    local useSpecial = function(line, type)
        if type == 1 then -- 1: logic | (!1): loop
            logic[#logic+1] = line
        else
            loop[#loop+1] = line
        end
        use(line)
    end
    -- Init the locals used in loop
    local currInstr, currCODE, currSBX
    repeat -- first run, get visible instr + postive jmps
        currInstr = instr[curr]
        currCODE, currSBX = currInstr[1], currInstr[2]['sBx']
        local addLoop = function(line)
            local currInstr = instr[line]
            local currCODE, currSBX = currInstr[1], currInstr[2]['sBx']
            if jumpOPs[currCODE] then
                useSpecial(line, 0)
                if currSBX >= 0 then
                    -- local lastPos = line
                    curr = line + currSBX + 1
                else
                    -- do record, but not jumping
                    nLoop[#nLoop+1] = line
                    curr = curr + 1
                end
                return true
            end
        end
        -- Logic Codes
        if logicOPs[currCODE] then
            useSpecial(curr, 1)
            local lastPos = curr
            if not addLoop(lastPos+1) then
                use(lastPos+1)
            end
            if not addLoop(lastPos+2) then
                use(lastPos+2)
            end
            curr = lastPos + 3
            goto skip
        end
        if not addLoop(curr) then
            -- Other visible OPs
            use(curr)
            curr = curr + 1
        end
        ::skip::
    until curr > count

    Alert("Loop: "..tostring(loop))
    Alert("nLoop: "..tostring(nLoop))
    Alert("logic: "..tostring(logic))
    Alert("used: "..tostring(used))

    curr = 1 -- reset
    -- Trun loop table to query mode
    -- local qLoop = {}
    -- for i = 1, #loop do
    --     qLoop[loop[i]] = i
    -- end
    -- Handle nLoop
    local patchNLoop = function (line)
        -- This function will load all unused instr until hit a loop op
        local currInstr = instr[line]
        local currCODE, currSBX = currInstr[1], currInstr[2]['sBx']
        local curr = line + currSBX + 1
        local currInstr, currCODE, currSBX
        local addLoop
        repeat
            currInstr = instr[line]
            currCODE, currSBX = currInstr[1], currInstr[2]['sBx']
            if checkUsed(line) then
                curr = curr + 1 
                goto skip
            end
            addLoop = function(line)
                local currInstr = instr[line]
                local currCODE, currSBX = currInstr[1], currInstr[2]['sBx']
                if jumpOPs[currCODE] then
                    useSpecial(line, 0)
                    if currSBX >= 0 then
                        -- local lastPos = line
                        curr = line + currSBX + 1
                    else
                        -- do record, but not jumping
                        nLoop[#nLoop+1] = line
                        curr = curr + 1
                    end
                    return true
                end
            end
            -- Logic Codes
            if logicOPs[currCODE] then
                useSpecial(curr, 1)
                local lastPos = curr
                if not addLoop(lastPos+1) then
                    use(lastPos+1)
                end
                if not addLoop(lastPos+2) then
                    use(lastPos+2)
                end
                curr = lastPos + 3
                goto skip
            end
            if not addLoop(curr) then
                -- Other visible OPs
                use(curr)
                curr = curr + 1
            end
            ::skip::
            if logicOPs[currCODE] then
                break
            end
        until curr > count
    end
    for idx = 1, #nLoop do
        patchNLoop(nLoop[idx])
        nLoop[idx] = nil
    end
    Alert("Loop: "..tostring(loop))
    Alert("nLoop: "..tostring(nLoop))
    Alert("logic: "..tostring(logic))
    Alert("used: "..tostring(used))
end

function this:main()
    local currLevel = Editor.currentLevel
    local info = Editor.protoLevel[currLevel]
    if info.State == Editor.States.DELETED or info.State == Editor.States.EMPTY then
        Toast(Language.err.ListIsEmpty)
        return
    end
    while true do
        local input = Choice({
            "全局: " ..
            Language.other[this.global == true and "Yes" or "No"][1],
            "開始還原"
        })
        if not input or input == 0 then
            return
        elseif input == 1 then
            this.global = not this.global
        elseif input == 2 then
            local targetProto = Editor.cacheProto[currLevel]
            local origInstr = targetProto.instr
            local cleanedInstr = ensureUsed(origInstr)
        end
    end
    return "close_plugin_UI"
end

return this