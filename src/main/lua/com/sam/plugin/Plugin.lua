---@class Plugin
Plugin = {avalible = {}}

-- ! -----------------------------------------------------------!
-- ! WARN: 此class並不會在正式開放中加入,只會在服務器上面運行(部分)!
-- ! -----------------------------------------------------------!

local pluginList = {
    -- "53To52",
    "Pseudocode", "Decompile", "RemoveZeroChunk", "GarbageCleaner", "CosntNameCleaner", "JumpReslover" ---? Isn't possible at the moment, consider a "static decompiler" will be better
}
local count = #pluginList

function Plugin:preinit(id)
    local i
    for curr = 1, count do
        i = id or curr
        local ok, package = pcall(require, "plugin." .. pluginList[i])
        if not ok then
            print(
                Language.plugin.Name .. " `" .. pluginList[i] .. "` " ..
                    Language.err.PluginLoadFailed)
            error(package)
        end
        local info = package.init()
        local core = setmetatable(package, {
            __index = function(_, key) return info[key] end,
            __newindex = function(tab, key, value)
                if info[key] ~= nil then
                    info[key] = value
                elseif tab[key] ~= nil then
                    tab[key] = value
                end
            end
        })
        if core.config then
            local settingsLoaded = 0
            if core.config_settings then
                for key, val in next, core.config_settings do
                    settingsLoaded = settingsLoaded + 1
                    if Config.Settings[key] == nil and val ~= nil then
                        Config.Settings[key] = val
                    end
                end
            end
            if not core.config_settings or settingsLoaded == 0 then
                core.config = false
            end
        end
        if core.language then
            local languagesLoaded = false
            if core.languages then
                if core.languages[Config.Settings.lang] ~= nil then
                    languagesLoaded = true
                    -- TODO
                end
            end
            if not core.languages or not languagesLoaded then
                core.language = false
            end
        end
        Plugin.avalible[(id or #Plugin.avalible + 1)] = core;
        if id then break end
    end
end

function Plugin:init() Plugin:preinit() end

local errorHandler = function(errMsg)
    local traceback = debug.traceback("Traceback for devs:")
    local errorMsgHead = "Plugin has occured an critial error!\n" ..
                             "Please report to the plugin developer with the following message:\n\n"
    local errorMsgBody = ("Error cuased: [%s]\n%s"):format(tostring(errMsg),
                                                           traceback)
    while true do
        local action = Alert(errorMsgHead .. errorMsgBody, "Copy error",
                             "print error", "Close")
        if action == 1 then
            gg.copyText(errorMsgBody)
        elseif action == 2 then
            print(errorMsgBody)
        elseif action == 3 then
            break
        end
    end
    return "close_plugin_UI"
end

function Plugin:showListUI()
    local avalible = Plugin.avalible
    local count = #avalible
    local UI = {}
    for i = 1, count do
        -- TODO add verified author tag
        UI[i] = avalible[i].name .. " (" .. avalible[i].author .. ")"
        if avalible[i].desc then
            UI[i] = UI[i] .. "\n(" .. avalible[i].desc .. ")"
        end
    end
    while true do
        local choice = Choice(UI, nil, Language.plugin.NameList)
        if not choice or choice == 0 then
            return
        elseif choice > 0 then
            if Config.defaultSettings.debug then
                while true do
                    local action = Choice({"熱更新模塊", "運行模塊"},
                                          nil,
                                          ("名稱: %s\n作者: %s\n簡介: %s\nID: %s (%i)"):format(
                                              avalible[choice].name,
                                              avalible[choice].author,
                                              avalible[choice].desc,
                                              avalible[choice].id, choice))
                    if action == 1 then
                        package.loaded["plugin."..avalible[choice].id] = nil
                        avalible[choice] = nil
                        Plugin:preinit(choice)
                    end
                    break
                end
            end
            local _, err = xpcall(avalible[choice].main, errorHandler)
            if err == "close_plugin_UI" then break end
        end
    end
end
