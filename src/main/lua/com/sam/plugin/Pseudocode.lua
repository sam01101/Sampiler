--[[    Plugin name: Pseudocode
    Alias folder: None
    Desc: 僞代碼
    Author: Sam
--]]
---@class Pseudocode
local this = {}
local toPseudo = require("plugin.decompile.pseudo.ToPseudo")
local variableFinder = require("plugin.decompile.pseudo.VariableFinder")
local upvalues = require("plugin.decompile.pseudo.Registers")

log = require("util.Log")("INFO")
local format = string.format

function this:init()
    return {
        id = "Pseudocode",
        name = "僞代碼",
        desc = "顯示/轉換成僞代碼",
        author = "Sam",
        config = false,
        config_settings = {

        },
    }
end

local getChunkInfo = function(level) -- TODO Empty chunk handle
    local currLevel = level or Editor.currentLevel
    local chunk_info = Editor.protoLevel[currLevel]
    if level == false and (chunk_info.State == Editor.States.DELETED or
    chunk_info.State == Editor.States.EMPTY) then
        Toast(Language.err.ListIsEmpty)
        return nil, nil
    end
    return currLevel, Editor.cacheProto[currLevel]
end

---@param proto Prototype
local init_trans = function(proto, level)
    ---@class LiteVM
    local vm = {}
    vm.pc = 1
    vm.level = level
    vm.root = proto.root
    vm.subLevel = proto.level
    vm.maxPC = #proto.instr
    vm.const = proto.const
    vm.upval = proto.upvalue
    vm.instr = proto.instr
    vm.param = proto.param
    vm.reg = proto.reg
    vm.vararg = proto.vararg > 0
    vm.func = proto.func
    --[[        Locals based on register param
    --]]
    vm.variable = variableFinder(proto, vm.param, vm.reg)
    vm.getLocalName = function(register)
        for i = 1, #vm.variable do
            if vm.variable[i].register == register then
                return vm.variable[i].name
            end
        end
        -- Find failed, use a temp name instead
        --print("Cannot find",register,vm.variable)
        return "R_" .. register
    end
    --[[        When lines are processed out of order, they are noted
        here so they can be skipped when encountered normally.
    --]]
    vm.skip = {}
    --[[        Store the label where jump has requested to jump on (If possible)
        A small note: This is pseudo code, not decompile.
    --]]
    vm.label = {}
    vm.jump = function(request, target)
        local tline = vm.instr[target]
        if tline then
            vm.label[tline.id] = target
        end
        return "goto line_" .. target .. ";"
    end

    vm.getConst = function(index)
        index = -index
        if vm.const[index] and vm.const[index].type then
            local display, _type = vm.const[index][1], vm.const[index].type
            if _type == "string" then
                for i = 1, 19 do
                    display = display:gsub(string.char(i), "\\" .. ("0"):rep(3 - #("" .. i)) .. i)
                end
                display = "\"" .. display .. "\""
            elseif _type == "number" then
                if display % 1 == 0 then
                    display = format("%.0f", display)
                end
            end
            return display, vm.const[index].type
        else
            log.warn("Unable to get constant " .. index)
            return nil, "nil"
        end
    end
    vm.getRK = function(index)
        if index >= 0 then
            return vm.getLocalName(index)
        elseif index < 0 then
            return vm.getConst(index)
        end
    end
    return vm -- Pointer
end

-- toPseudo
---@param vm LiteVM
local trans = function(vm, root)
    local codes = {}
    local reg_init = {}
    for i = 1, #vm.variable do
        ---@type Declaration
        local curr = vm.variable[i]
        if not curr.is_arg then
            table.insert(reg_init, curr.register)
        end
    end
    --print("Reg init | min:",reg_init[1], "max:",reg_init[#reg_init] - reg_init[1])
    for pc = 1, vm.maxPC do
        local OP, instr_args = vm.instr[pc][1], vm.instr[pc][2]
        codes[#codes + 1] = {
            id = vm.instr[pc].id
        }
        vm.pc = pc + 1
        if not vm.skip[pc + 1] then
            codes[#codes][1] = toPseudo[OP](vm, instr_args, root)
        end
    end
    vm.code = {}
    for i = 1, #codes do
        if vm.label[codes[i].id] then
            if codes[i][1] then
                table.insert(codes[i], 1, ("::line_%s::"):format(vm.label[codes[i].id]))
            else
                codes[i][1] = ("::line_%s::"):format(vm.label[codes[i].id])
            end
        end
        if codes[i][1] then
            for ii = 1, #codes[i] do
                vm.code[#vm.code + 1] = codes[i][ii]
            end
        end
    end
    if #reg_init > 0 then
        reg_init = {
            A = reg_init[1];
            B = reg_init[#reg_init] - reg_init[1];
        }
        reg_init = toPseudo["LOADNIL"](vm, reg_init, true)
        table.insert(vm.code, 1, reg_init)
    end
    reg_init = nil
    return vm
end

function this:main()
    local chunks = {}
    while true do
        local options = Choice({
            "轉換",
            "顯示"
        }, nil, "請選擇僞代碼模式\n注: 僞代碼只供參考, 能不能執行全靠運氣")
        if options then
            local range = Choice({
                "局部",
                "全局"
            }, nil, "請選擇範圍")
            if options == 1 then
                if range == 1 then -- 局部
                    local level
                    ---@type Prototype
                    local chunk
                    level, chunk = getChunkInfo(false)
                    if not level then break end
                    local lite_vm = init_trans(chunk)
                    chunks[level] = trans(lite_vm)
                    Alert(table.concat(chunks[level].code, "\n"), "")
                elseif range == 2 then -- 全局
                    local levels = Editor.protoLevel
                    local _, level
                    ---@type Prototype
                    local chunk
                    local createFunction = function(_level)
                        level, chunk = getChunkInfo(_level)
                        local lite_vm = init_trans(chunk, level)
                        local traceUpvalue;
                        ---@param self LiteVM
                        traceUpvalue = function(self, currLevel, currUpvalue)
                            local rootLevel = currLevel ~= 1 and Editor.cacheProto[currLevel].root or 0
                            ---@type LiteVM
                            local rootProto = chunks[rootLevel]
                            local rootLocal, rootUpval
                            if rootProto then
                                rootLocal, rootUpval = rootProto.variable, rootProto.upval
                            end
                            -- Where am I? [ ] currLevel | rootLevel [<]
                            if currUpvalue.instack then -- Local (No need traceback)
                                if currLevel == 1 and currUpvalue.position == 0 then
                                    return "_ENV"
                                end
                                if not rootLocal[currUpvalue.position + 1] then
                                    print("error", currLevel, rootLevel, currUpvalue.position + 1, rootLocal)
                                end
                                return rootLocal[currUpvalue.position + 1].name
                            else -- Upvalue (May need traceback)
                                return traceUpvalue(self, rootLevel, rootUpval[currUpvalue.position + 1])
                            end
                        end
                        chunks[_level] = trans(lite_vm, traceUpvalue)
                    end
                    for i = 1, #levels do
                        if levels[i].State == Editor.States.EMPTY then
                            createFunction(i)
                            chunks[i].code = { "-- Nothing here :(" }
                        elseif levels[i].State == Editor.States.DELETED then
                            chunks[i].code = { "-- Deleted chunk" }
                        elseif levels[i].State == Editor.States.EXIST then
                            createFunction(i)
                        end
                    end
                    local func_head = "local function FUNC_%i(%s)\n%s"
                    local func_end = "\n%send"
                    local addFunc;
                    addFunc = function(lev, space_level)
                        ---@type LiteVM
                        local code, chunkInfo = chunks[lev].code, chunks[lev]
                        local func_input = {}
                        if chunkInfo.param > 0 then
                            for i = 1, chunkInfo.param do
                                table.insert(func_input, chunkInfo.variable[i].name)
                            end
                        end
                        if chunkInfo.vararg then
                            table.insert(func_input, "...")
                        end
                        local proto = Editor.cacheProto[lev]
                        local count = #proto.func
                        if count > 0 then
                            for i = 1, count do
                                local id = proto.func[i].id
                                local subFunc = addFunc(id, space_level + 1)
                                table.insert(code, 2, subFunc)
                            end
                        end
                        local space_lev = (" "):rep(4 * space_level)
                        if lev == 1 then -- %main
                            return table.concat(code, "\n")
                        end
                        return func_head:format(lev, table.concat(func_input, ", "), space_lev) ..
                        table.concat(code, "\n" .. space_lev) ..
                        func_end:format((" "):rep(4 * (space_level - 1)))
                    end
                    io.open("/sdcard/test.out.lua", "w+"):write(addFunc(1, 0)):close()
                end
            elseif options == 2 then
                -- TODO Add global range
            end
        else
            break
        end
    end
    chunks = nil
    return "close_plugin_UI"
end

return this