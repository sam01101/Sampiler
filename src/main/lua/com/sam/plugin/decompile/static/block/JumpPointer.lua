local line, count = 1, 0
local pointer = {};
local log = require "util.Log"("WARN")
local globalTree;
local pointers = {
    JMP    = true,
    FORLOOP = true,
    FORPREP = true,
    TFORLOOP = true;
}
local getId = function(instr_num)
    return globalTree.instr[instr_num].id
end

-- TODO verify is JMP vaild or not
local addPointer
addPointer = function(instr_num, target, garbage)
    local recursivePointer  = pointers[globalTree.instr[target][1]];
    if recursivePointer then
        -- Find the real dist
        log.debug("Recursive Pointer, real target is "..target)
        line = target
        local currInstr = globalTree.instr[target]
        local currCODE, currSBX = currInstr[1], currInstr[2].sBx or 0
        local dist = line + currSBX + 1
        addPointer(line, dist, dist > count or dist < 0)
        return
    end
    log.debug("請求:" .. instr_num .. " | 目標:" .. target)
    pointer[#pointer + 1] = {
        request = getId(instr_num),
        target  = garbage and "null" or getId(target),
        garbage = garbage,
        debug = {
            request = instr_num,
            target = target
        }
    }
end

local loadJumpPointer = function(instr)
    count = #instr
    repeat
        local currInstr = instr[line]
        local currCODE, currSBX = currInstr[1], currInstr[2].sBx or 0
        if pointers[currCODE] then
            local dist = line + currSBX + 1
            addPointer(line, dist, dist > count or dist < 0)
        end
        line = line + 1
    until line > count
end

local Init = function(tree)
    globalTree = tree
    log.info("开始加载跳跃指令的指针")
    loadJumpPointer(tree.instr);
    log.info('加载跳跃指令的指针完成, 共加载了 ' .. #pointer .. ' 个指针')
    --log.debug(pointer)
    local ret = pointer
    line, count, pointer = 1, 0, {}
    return ret
end

return Init