---@type Prototype
---@param header table
---@param main Prototype
---@param levels number
local Prototype;

local boolean = {[0] = 1, [1] = 1 }
local constType = { NIL = 0, BOOL = 1, NUM = 3, STR = 4 }
-- Number in 5.3
constType.NUMFLT = constType.NUM
constType.NUMINT = 19 -- NUM | (1 << 4)
-- String in 5.3
constType.SHRSTR = constType.STR
constType.LNGSTR = 20 -- STR | (1 << 4)

local io = io
local io_seek
local io_read
local io_input = io.input

local string = string
local string_byte = string.byte

local math = math
local math_floor = math.floor
local math_ldexp = math.ldexp

local os = os
local exit = os.exit

local print = print

local totalLevel = 0 -- Start from %main

-- Useful function
local pushError = function(msg)
    print(' >> -1:' .. io_seek() .. ' ' .. msg)
    print(debug.traceback())
    exit(1)
end

local get = function(num)
    local len = num or 1
    return io_read(len)
end

local getByte = function(num)
    local len = num or 1
    local cache = io_read(len)
    if not cache then
        pushError("Attempt to get " .. len .. " byte")
    end
    local ret = { string_byte(cache, 0, -1) }
    return len > 1 and ret or ret[1]
end

--[[local getHex = function(num) -- unused
    num = num or 1
    local ret = getByte(num)
    local result;
    for forVal1 = 1, #ret do
        if num > 1 then
            result = (result or "") .. string_format('%02X', ret[forVal1])
        else
            return string_format('%02X', ret)
        end
    end
    return result
end
--]]
local getFourBit = function()
    local val = 256
    local ret = getByte(4)
    local a, b, c, d = ret[1], ret[2], ret[3], ret[4]
    if not (a and b and c and d) then
        pushError('Attempt to get 4 byte')
    end
    if a + b + c + d == 0 then
        return 0
    end
    return a + val * (b + val * (c + 256 * d))
end

local lua_NUMBER = function(x)
    local sign = 1
    local mantissa = string_byte(x, 7) % 16
    for i = 6, 1, -1 do
        mantissa = mantissa * 256 + string_byte(x, i)
    end
    if string_byte(x, 8) > 127 then
        sign = -1
    end
    local exponent = (string_byte(x, 8) % 128) * 16 + math_floor(string_byte(x, 7) / 16)
    if exponent == 0 then
        return 0
    end
    mantissa = (math_ldexp(mantissa, -52) + 1) * sign
    return math_ldexp(mantissa, exponent - 1023)
end

-----------------------------------------------------------------------
-- Converts a little-endian integer string to a number
-- Source: ChunkSpy, untested
-----------------------------------------------------------------------
local lua_INT = function(x, size_int)
    size_int = size_int or 8
    local sum = 0
    local highestbyte = string_byte(x, size_int)
    if highestbyte <= 127 then
        sum = highestbyte
    else
        sum = highestbyte - 256
    end
    for i = size_int - 1, 1, -1 do
        sum = sum * 256 + string_byte(x, i)
    end
    return sum
end

local getStringLength = function()
    local len = getByte()
    local isLongString = nil
    if len == 255 then
        isLongString = true
        local ret = getByte(Prototype.header.sizet)
        local a, b, c, d, e, f, g, h = ret[1], ret[2], ret[3], ret[4], ret[5], ret[6], ret[7], ret[8]
        if not (a and b and c and d and e and f and g and h) then
            pushError('Attempt to get 8 byte')
        end
        if a + b + c + d + e + f + g + h == 0 then
            return 0
        end
        len = 0
        for i = Prototype.header.sizet, 1, -1 do
            len = len * 256 + ret[i]
        end
    end
    return len
end

local getString = function()
    local len = getStringLength()
    if len == 0 then return '' end
    local ret = get(len - 1)
    --[[    local replace = {"b", "t", "n", "v", "f", "r"}
    for i = 0, 7 do
        if string_find(ret, string_char(i)) then
            ret = string_gsub(ret, string_char(i), "\\" .. i)
        end
    end
    for i = 8, 8 + #replace do
        if string_find(ret, string_char(i)) then
            ret = string_gsub(ret, string_char(i), "\\" .. replace[i - 7])
        end
    end
    --]]
    return ret
end

local setFunction = function(name, s)
    return function(...)
        return s[name](s, ...)
    end
end

local defineIO = function(steam)
    io_read = setFunction('read', steam)
    io_seek = setFunction('seek', steam)
end

local expected = function(msg, curr, orig)
    if type(orig) == 'table' then
        if not orig[curr] then
            pushError(msg .. ' expected, got (' .. curr .. ')')
        end
    elseif curr ~= orig then
        pushError(msg .. '(' .. orig .. ') expected, got (' .. curr .. ')')
    end
    return curr
end

-- Main
local readHeader = function()
    local lua_SIGNATURE = '\027Lua\x53' -- 5
    local lua_TAIL = '\x19\x93\r\n\026\n' -- 6
    return {
        sign = expected('lua signature', get(5), lua_SIGNATURE),
        RIO = expected('offical chunk', getByte(), 0),
        -- endian = expected('endian/little-endian', getByte(), boolean) == 0,
        tail = expected('lua tail (LUAC_DATA)', get(6), lua_TAIL),
        int = expected('normal integers length', getByte(), 4),
        sizet = expected('normal size_t', getByte(), {[4] = true, [8] = true }),
        instr = expected('normal instructions length', getByte(), 4),
        --new lua_Integer
        numint = expected('normal lua_INTEGAR', getByte(), 8),
        numlen = expected('normal lua_NUMBER', getByte(), 8),
        -----------------------------------------------------------------
        -- float = expected('no floating point', getByte(), 0),
        LUAC_INT = expected('LUAC_INT', lua_INT(get(8)), 22136),
        LUAC_NUM = expected('LUAC_NUM', lua_NUMBER(get(8)), 370.5),
        NUpval = getByte()
    }
end

local readPrototype
local readInstructions
local readConstants
local readFunctions
local readUpvalues
local readDebug

readPrototype = function(currLevel)
    local source = getString()
    totalLevel = totalLevel + 1
    return {
        level = currLevel,
        linedefined = getFourBit(),
        lastlinedefined = getFourBit(),
        param = getByte(),
        vararg = getByte(),
        reg = getByte(),
        instr = readInstructions(),
        const = readConstants(),
        upvalue = readUpvalues(),
        func = readFunctions(),
        extra = readDebug(source)
    }
end

readInstructions = function()
    local len = getFourBit()
    local disassemble, opcode
    local instr = {}
    if len < 1 then
        return instr
    end
    for i = 1, len do
        instr[i] = Bytecode:getReadable(getFourBit())
    end
    return instr
end

readConstants = function()
    local len = getFourBit()
    local const = {}
    local tmp = {}
    local types
    if len < 1 then
        return const
    end
    for i = 1, len do
        types = getByte()
        if types == constType.NIL then
            tmp = nil
        elseif types == constType.NUMFLT then
            tmp = lua_NUMBER(get(8))
        elseif types == constType.NUMINT then
            tmp = lua_INT(get(8))
        elseif types == constType.SHRSTR or types == constType.LNGSTR then
            tmp = getString()
        elseif types == constType.BOOL then
            tmp = getByte() > 0
        else
            pushError('Unknown constant type (' .. types .. ')')
        end
        const[i] = { type = type(tmp), tmp }
    end
    return const
end

readFunctions = function()
    local subLevel = 1
    local len = getFourBit()
    local func = {}
    if len < 1 then
        return func
    end
    for i = 1, len do
        func[i] = readPrototype(subLevel)
        subLevel = subLevel + 1
    end
    return func
end

readUpvalues = function()
    local len = getFourBit()
    local upvalue = {}
    if len < 1 then
        -- Weird exception
        return upvalue
    end
    for i = 1, len do
        upvalue[i] = {
            instack = expected('boolean', getByte(), boolean) == 1,
            position = getByte()
        }
    end
    return upvalue
end

readDebug = function(source)
    local len
    local debug = {}
    debug.source = source
    len = getFourBit()
    if len > 0 then
        debug.linenumber = {}
        for i = 1, len do
            debug.linenumber[i] = getFourBit()
        end
    end
    len = getFourBit()
    if len > 0 then
        debug.locals = {}
        for i = 1, len do
            debug.locals[i] = {
                name = getString(),
                startpc = getFourBit(),
                endpc = getFourBit()
            }
        end
    end
    len = getFourBit()
    if len > 0 then
        debug.upval = {}
        for i = 1, len do
            debug.upval[i] = getString()
        end
    end

    return debug
end

local function main(steam)
    steam = io_input(steam)
    if not steam then
        print("Can't open steam")
        exit(1)
    end
    defineIO(steam)
    io_seek('set', 0)
    Prototype = { header = readHeader() }
    Prototype.main = readPrototype(totalLevel)
    Prototype.levels = totalLevel
    return Prototype
end

Protoinit = main