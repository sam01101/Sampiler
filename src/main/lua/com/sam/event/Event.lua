--[[    AKA EDA(?) or something like that?
    This is a temp class for just a holder, I'll re-code for a offical format
    later, but let me just handle it for now.
    TODO https://juejin.im/entry/6844903680525729800
--]]
Event = {
    events = {},
    registered = {}
}
--[[    Status:
    1 = Hooked and ready
    0 = Deleted
--]]
function Event:init()

end

---添加一個Hook
function Event.add(eventName, func)
    if not Event.registered[eventName] then
        return false
    end
    Event.events[#Event.events + 1] = {
        call = func,
        hooked = eventName,
        status = 1
    }
    return #Event.events
end

---注冊一個Event
---@return number
function Event.register(funcName)
    Event.registered[funcName] = true
end

---Event被調用, 尋找可用的hook並調用
---@param invokeName string
function Event.invoke(invokeName, ...)
    if Event.registered[invokeName] then
        for i = 1, #Event.events do
            if Event.events[i].hooked == invokeName and Event.events[i].status == 1 then
                local result = Event.events[i].call(...)
                if result ~= nil then
                    return true, result
                end
                return false
            end
        end
    else
        return false, "Invaild event to invoke"
    end
end


---取消一個 hook | Sucess = true, Failed = false
---@return boolean
function Event.unregister(id)
    if Event.events[id] and Event.events[id].status == 1 then
        Event.events[id].status = 0
        return true
    end
    return false
end