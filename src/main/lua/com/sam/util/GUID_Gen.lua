local string_sub = string.sub
local format = string.format
local math_random = math.random
local seed = { 'e', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' }
return function()
    -- math.randomseed(math.random(1, 1999999))
    local sid = ""
    for _ = 1, 32 do
        sid = sid .. seed[math_random(1, 16)]
    end
    return format(
    '%s-%s-%s-%s-%s',
    string_sub(sid, 1, 8),
    string_sub(sid, 9, 12),
    string_sub(sid, 13, 16),
    string_sub(sid, 17, 20),
    string_sub(sid, 21, 32)
    )
end