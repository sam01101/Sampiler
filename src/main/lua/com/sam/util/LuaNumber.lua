--[[    -- Number handling functions (Taken form: Chunk Spy)
    -- * converts a string of bytes to and from a proper number
    -- ! WARNING single() and double() can only do normal floats
    -- ! and zeros. Denormals, infinities and NaNs are not recognized.
    -- * See 5.0.2/ChunkSpy.lua for IEEE floating-point notes
--]]
local char = string.char
local string_byte = string.byte

local math_floor = math.floor
local math_ldexp = math.ldexp
local math_frexp = math.frexp

local convert_from = {}       -- tables for number conversion function lookup
local convert_to = {}

-----------------------------------------------------------------------
-- support function for convert_to functions
-----------------------------------------------------------------------
local function grab_byte(v)
    return math_floor(v / 256), char(math_floor(v) % 256)
end

-----------------------------------------------------------------------
-- Converts an 8-byte little-endian string to a IEEE754 double number
-- * NOTE: see warning about accuracy in the header comments!
-----------------------------------------------------------------------
local function convert_from_double(x)
    local sign = 1
    local mantissa = string_byte(x, 7) % 16
    for i = 6, 1, -1 do mantissa = mantissa * 256 + string_byte(x, i) end
    if string_byte(x, 8) > 127 then sign = -1 end
    local exponent = (string_byte(x, 8) % 128) * 16 +
    math_floor(string_byte(x, 7) / 16)
    if exponent == 0 then return 0 end
    mantissa = (math_ldexp(mantissa, -52) + 1) * sign
    return math_ldexp(mantissa, exponent - 1023)
end
convert_from["double"] = convert_from_double

-----------------------------------------------------------------------
-- Converts a 4-byte little-endian string to a IEEE754 single number
-- * TODO UNTESTED!!! *
-----------------------------------------------------------------------
local function convert_from_single(x)
    local sign = 1
    local mantissa = string_byte(x, 3) % 128
    for i = 2, 1, -1 do mantissa = mantissa * 256 + string_byte(x, i) end
    if string_byte(x, 4) > 127 then sign = -1 end
    local exponent = (string_byte(x, 4) % 128) * 2 +
    math_floor(string_byte(x, 3) / 128)
    if exponent == 0 then return 0 end
    mantissa = (math_ldexp(mantissa, -23) + 1) * sign
    return math_ldexp(mantissa, exponent - 127)
end
convert_from["single"] = convert_from_single

-----------------------------------------------------------------------
-- Converts a little-endian integer string to a number
-- * TODO UNTESTED!!! *
-----------------------------------------------------------------------
local function convert_from_int(x, size_int)
    size_int = size_int or config.size_lua_Number or 4
    local sum = 0
    for i = size_int, 1, -1 do
        sum = sum * 256 + string_byte(x, i)
    end
    -- test for negative number
    if string_byte(x, size_int) > 127 then
        sum = sum - math_ldexp(1, 8 * size_int)
    end
    return sum
end
convert_from["int"] = convert_from_int

-----------------------------------------------------------------------
-- * WARNING this will fail for large long longs (64-bit numbers)
--   because long longs exceeds the precision of doubles.
-----------------------------------------------------------------------
convert_from["long long"] = convert_from_int

-----------------------------------------------------------------------
-- Converts a IEEE754 double number to an 8-byte little-endian string
-- * NOTE: see warning about accuracy in the header comments!
-----------------------------------------------------------------------
convert_to["double"] = function(x)
    local sign = 0
    if x < 0 then sign = 1; x = -x end
    local mantissa, exponent = math_frexp(x)
    if x == 0 then -- zero
        mantissa, exponent = 0, 0
    else
        mantissa = (mantissa * 2 - 1) * math_ldexp(0.5, 53)
        exponent = exponent + 1022
    end
    local v, byte = "" -- convert to bytes
    x = mantissa
    for i = 1, 6 do
        x, byte = grab_byte(x); v = v .. byte -- 47:0
    end
    x, byte = grab_byte(exponent * 16 + x); v = v .. byte -- 55:48
    x, byte = grab_byte(sign * 128 + x); v = v .. byte -- 63:56
    return v
end

-----------------------------------------------------------------------
-- Converts a IEEE754 single number to a 4-byte little-endian string
-- * TODO UNTESTED!!! *
-----------------------------------------------------------------------
convert_to["single"] = function(x)
    local sign = 0
    if x < 0 then sign = 1; x = -x end
    local mantissa, exponent = math_frexp(x)
    if x == 0 then -- zero
        mantissa = 0; exponent = 0
    else
        mantissa = (mantissa * 2 - 1) * math_ldexp(0.5, 24)
        exponent = exponent + 126
    end
    local v, byte = "" -- convert to bytes
    x, byte = grab_byte(mantissa); v = v .. byte -- 7:0
    x, byte = grab_byte(x); v = v .. byte -- 15:8
    x, byte = grab_byte(exponent * 128 + x); v = v .. byte -- 23:16
    x, byte = grab_byte(sign * 128 + x); v = v .. byte -- 31:24
    return v
end

-----------------------------------------------------------------------
-- Converts a number to a little-endian integer string
-- * TODO UNTESTED!!! *
-----------------------------------------------------------------------
convert_to["int"] = function(x)
    local v = ""
    x = math_floor(x)
    if x >= 0 then
        for i = 1, config.size_lua_Number do
            v = v .. char(x % 256); x = math_floor(x / 256)
        end
    else-- x < 0
        x = -x
        local carry = 1
        for i = 1, config.size_lua_Number do
            local c = 255 - (x % 256) + carry
            if c == 256 then c = 0; carry = 1
            else carry = 0 end
            v = v .. char(c); x = math_floor(x / 256)
        end
    end
    -- optional overflow test; not enabled at the moment
    -- if x > 0 then error("number conversion overflow") end
    return v
end

-----------------------------------------------------------------------
-- * WARNING this will fail for large long longs (64-bit numbers)
--   because long longs exceeds the precision of doubles.
-----------------------------------------------------------------------
convert_to["long long"] = convert_to["int"]

local function getNumberFunc(types)
    return convert_from[types], convert_to[types]
end
return getNumberFunc