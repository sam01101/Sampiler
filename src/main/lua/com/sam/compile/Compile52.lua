local endian, header;
local BinConstantType = {
    ["nil"] = "\0",
    boolean = "\1",
    number = "\3",
    string = "\4"
};

local bit32 = bit32 or require("bit32");
local numConv, convFunc = require("util.LuaNumber");

local string = string;
local char = string.char
local reverse = string.reverse

local math = math;
local math_floor = math.floor

local write;

-------------------------------------------------------------
-- writes an unsigned integer (for integers, size_ts)
-- * unsigned because the relevant data is always positive
-------------------------------------------------------------
local toUnsigned = function(num, type_size)
    local orig = num
    if not type_size then type_size = header.int end
    local v = ""
    for i = 1, type_size do
        v = v .. char(num % 256);
        num = math_floor(num / 256)
    end
    if endian then
        return reverse(v)
    end
    return v
end

local lua_NUMBER = function(x)
    local result = convFunc(x)
    if endian then
        return reverse(result)
    end
    return result
end

local setFunction = function(name, s)
    return function(...) return s[name](s, ...) end
end

-- Main

local writePrototype;
local writeInstructions;
local writeConstants;
local writeFunctions;
local writeUpvalues;
local writeDebug;

writePrototype = function(tree)
    if not Config.Settings.strip then
        write(toUnsigned(0), toUnsigned(0))
    else
        write(toUnsigned(tree.linedefined), toUnsigned(tree.lastlinedefined))
    end
    write(char(tree.param), char(tree.vararg), char(tree.reg))
    writeInstructions(tree.instr)
    writeConstants(tree.const)
    writeFunctions(tree.func)
    writeUpvalues(tree.upvalue)
    writeDebug(tree.extra)
end

writeInstructions = function(instr)
    local instrlen = #instr
    write(toUnsigned(instrlen))
    for i = 1, instrlen do write(toUnsigned(Bytecode:toBinary(instr[i]))) end
end

writeConstants = function(constant)
    local str, types;
    local constlen = #constant;
    write(toUnsigned(constlen))
    for i = 1, constlen do
        types = constant[i].type;
        write(BinConstantType[types])
        if types == "nil" then

        elseif types == "boolean" then
            write(char(constant[i][1] and 1 or 0))
        elseif types == "number" then
            write(lua_NUMBER(constant[i][1]))
        elseif types == "string" then
            str = constant[i][1] .. "\0" -- load("return '" .. constant[i][1] .. "\0'")();
            write(toUnsigned(#str), str);
        elseif types ~= "nil" and types ~= "boolean" and types ~= "number" and
            types ~= "string" then
            error("Unknown const type " .. (types or "null"))
        end
    end
end

writeFunctions = function(func)
    local funclen = #func
    write(toUnsigned(funclen))
    for i = 1, funclen do writePrototype(func[i]) end
end

writeUpvalues = function(upval)
    local upvaluelen = #upval
    local curr;
    write(toUnsigned(upvaluelen))
    for i = 1, upvaluelen do
        curr = upval[i]
        write(char(curr.instack and 1 or 0), char(curr.position))
    end
end

writeDebug = function(debug)
    if not Config.Settings.strip then
        for i = 1, 16 do write(char(0)) end
        return
    end
    if debug.source and #debug.source > 0 then
        write(toUnsigned(#debug.source + 1))
        write(debug.source .. "\0")
    elseif not debug.source or #debug.source <= 0 then
        write(char(0):rep(4))
    end
    if debug.linenumber and #debug.linenumber > 0 then
        write(toUnsigned(#debug.linenumber))
        for i = 1, #debug.linenumber do
            write(toUnsigned(debug.linenumber[i]))
        end
    elseif not debug.linenumber or #debug.linenumber <= 0 then
        write(char(0):rep(4))
    end
    if debug.locals and #debug.locals > 0 then
        write(toUnsigned(#debug.locals))
        for i = 1, #debug.locals do
            local curr_loc = debug.locals[i]
            write(toUnsigned(#curr_loc.name + 1), curr_loc.name .. "\0",
                  toUnsigned(curr_loc.startpc), toUnsigned(curr_loc.endpc))
        end
    elseif not debug.locals or #debug.locals <= 0 then
        write(char(0):rep(4))
    end
    if debug.upval and #debug.upval > 0 then
        write(toUnsigned(#debug.upval))
        for i = 1, #debug.upval do
            local curr_upval = debug.upval[i]
            write(toUnsigned(#curr_upval + 1), curr_upval .. "\0")
        end
    elseif not debug.upval or #debug.upval <= 0 then
        write(char(0):rep(4))
    end
end

---@param tree table
---@param output userdata
local function main(tree, output)
    write = setFunction("write", output);
    local close = setFunction("close", output);
    local _;
    header = tree.header
    endian = not header.endian;
    _, convFunc = numConv(header.numType)
    write(header.sign, char(header.RIO), char(endian and 0 or 1),
          char(header.int), char(header.instr), char(header.sizet),
          char(header.numlen), char(header.float), header.tail);
    writePrototype(tree.main)
    tree.header, header, tree.levels = nil;
    close();
end

Compile = main
