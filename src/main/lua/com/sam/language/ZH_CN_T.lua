local cfg = {}

cfg.input = {
    InputFile = "請選擇需要分析的Lua 5.2檔案:",
    EditType = "請選擇類型修改"

}

cfg.settings = {
    Name = "設置",
    ConstantEditPath = "請選擇常量臨時路徑(日後可在\"設置\"修改):",
    langName = "語言 (Language)",
    inputFileName = "默認輸入路徑",
    ConstEditPathName = "常量臨時路徑",
    NoConstTipsName = "修改常量提示",
    stripName = "保留調試信息",
    ListenFileName = "觀察在本地編輯的檔案並提示是否修改",
    debugName = "調試模式"
}

cfg.plugin = {
    Name = "插件",
    NameList = "插件列表",
    FiveThreeToFiveTwoName = "轉換5.3版本到GG 5.2版本",
    HotUpdate = "熱更新"
}

cfg.natural = {
    Analyzing = "正在分析...",
    OutputProcessing = "正在輸出...",
    ProcessExecuteSuccess = "該操作已成功執行!",
    AnalyzeSuccess = "解析成功",
    OutputFileSuccess = "輸出成功!"
}

cfg.other = { -- Give up XD
    DeleteProtoInstr = "刪除此區塊全部指令",
    Back = "返回",
    Dismiss = "不再提示",
    Yes = { "是", "有", "確定" },
    No = { "否", "沒有" },
    Length = "長度",
    Done = "完成",
    DropEdit = "放棄編輯",
    Is = "的",
    List = "列表",
    Position = "位置",
    Empty = "空",
    Deleted = "已刪除",
    Name = "名稱",
    Type = "類型",
    Exit = "退出",
    Start = "開始",
    ConstCreatedHead = "文件已創建在路徑",
    ConstCreatedTail = "你現在可以打開編輯器/文件管理器修改",
}

cfg.tips = {
    PressBack = "按__cancel__返回",
    IsInputNewFile = "是否繼續分析新腳本?",
    IfDeviceSlow = "如果設備性能較差可能需要較長時間", --! May change to IfNetworkSlow
    EditConst = "如何修改常量 v0.1\n\n\n" ..
    "為了方便對常量進行更多操作,\n" ..
    "此腳本會要求用戶指定一個路徑\n\n" ..
    "當用戶觸發修改常量請求時, 腳本\n" ..
    "會輸出該常量到指定路徑並創建一個臨時文件\n\n" ..
    "此時用戶可以利用外部文件管理器/編輯器去\n" ..
    "修改或者進行更多需要的操作.",
    ForceDataTypeChangeWarn = "警告\n\n" ..
    "如果執行此操作, 有可能導致數據損壞!\n\n" ..
    "原因: 類型不一\n\n" ..
    "是否繼續?"
}

cfg.output = {
    OutputFileName = "請填寫輸出的腳本名稱:",
    OutputFilePath = "請選擇輸出的路徑:",
    KeepDebugInfo = "保留調試信息?"
}

cfg.err = {
    InvaildFile = "文件無法打開/不存在",
    InstrReadFail = "指令讀取錯誤, 請檢查一次原型是否正常的",
    InvaildPath = "路徑無效",
    InvaildNumber = "數字無效",
    UndoFailed = "想回去? 重新開一次 (無慈悲)",
    ReadOnly = "目前只能查看, 並不能修改.",
    UnEditable = "無法修改!",
    ListIsEmpty = "列表為空!",
    UnknownType = "未知類型",
    PluginLoadFailed_User = "插件加載失敗!",
    PluginLoadFailed = "加載失敗!"
}

cfg.proto = {
    ProtoHeader = "區塊開頭",
    ProtoInfo = { "區塊內容", "函數信息" },
    TypeDebug = "調試信息",
    ConstantList = "常量表",
    InstrList = "指令表",
    ChangeProto = "切換區塊",
    CompileProto = "編譯區塊"
}

cfg.proto.type = {
    Name = "類型",
    Number = "數字",
    String = "文字",
    Boolean = "布爾",
}

cfg.proto.header = {
    LuaSign_Ver = "Lua開頭簽名",
    LuaTail = "Lua結尾(用於檢查EOL轉換)",
    IsOffical = "是否為官方編譯",
    IsEndian = "是否為小字節端",
    SizeofInt = "int大小",
    SizeofT = "指針(Size_t)大小",
    SizeofInstr = "指令大小",
    SizeofNumber = "常量數字大小",
    IsUsingInt = "是否用整數來表示數字"
}

cfg.proto.data = {
    NumberOfArg = "參數數量",
    IsVararg = "是否有vararg(...)",
    MaxStackSize = "最大寄存器數量",
}

cfg.proto.instr = {
    Name = { "指令", "字節碼" },
    NumberOfInstr = "指令數量",
    CountInstr = "指令總數",
    CurrentInstr = "目前指令",
    SelectNewInstr = "請選擇替換的指令"
}

cfg.proto.const = {
    NumberOfConstant = "常量數量",
    CountConstant = "常量總數"
}

cfg.proto.upval = {
    NumberOfUpvalue = "Upvalue數量",
    CountUpvalue = "Upvalue總數",
    IsInStack = "是否在棧區(instack)"
}

cfg.proto.funcs = {

    NumberOfProtos = "次區塊數量",
    CountProtos = "區塊總數"
}

cfg.proto.debug = {
    SourceString = "源碼信息",
    LineNumber = "源碼位置",
    LocalNameList = "局部變量名稱列表",
    UpvalueList = "Upvalue列表",
    LineDefined = "定義開始行",
    LastLineDefined = "定義結束行"
}

cfg.proto.decompile = {
    TryDecompile = "嘗試反編譯"
}

return cfg