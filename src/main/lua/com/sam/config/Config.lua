---@class Config
Config = {}

--- 默認設置
Config.defaultSettings = {
    lang = nil,
    inputFile = "/sdcard",
    ConstEditPath = nil,
    NoConstTips = false,
    strip = true,
    ListenFile = false,
    debug = true
}

local this = Config

--- 添加設置到臨時table
---@param key string
---@param value any
---@return Config
function this:addSettings(key, value)
    if not this.Settings then
        this.Settings = this:getSettings()
    end
    this.Settings[key] = value
    return this
end

--- 將設置轉成二進制文件
---@return string binary
function this:dumpSettings()
    if not this.Settings then
        error("设置不存在!")
    end
    local outString = "return{"
    for k, v in pairs(this.Settings) do
        outString = outString .. k .. " = "
        if type(v) == "string" then
            outString = outString .. "_(" .. table.concat({ v:byte(0, -1) }, ",") .. ")"
        elseif type(v) == "boolean" or type(v) == "nil" or type(v) == "number" then
            outString = outString .. tostring(v)
        else
            outString = outString .. v
        end
        outString = outString .. ","
    end
    outString = outString .. "}"
    return string.dump(load(outString), true)
end
--- 創建新設置(+文件) 并保存默认设置
--- 默認路徑: gg.EXT_FILES_DIR
---@return Config
function this:newConfig()
    local config, err = io.open(gg.EXT_FILES_DIR .. "/Sam's Tool.cfg", "w+")
    if not config then
        error("創建Config失敗, 原因:\n" .. tostring(err))
    end
    -- 使用默认设置
    this.Settings = this.defaultSettings
    config:write(this:dumpSettings())
    config:close()
    return this
end

--- 保存設置
---@return Config
function this:saveConfig()
    local config, err = io.open(gg.EXT_FILES_DIR .. "/Sam's Tool.cfg", "w+")
    if not config then
        error("創建Config失敗, 原因:\n" .. tostring(err))
    end
    config:write(this:dumpSettings())
    config:close()
    return this
end

--- 嘗試獲取已儲存的設置, 如果有返回function
---@return function|nil
function this:tryConfig()
    return loadfile(gg.EXT_FILES_DIR .. "/Sam's Tool.cfg")
end

--- 嘗試獲取設置, 如果不存在創造新設置, 否則使用該設置
---@return Config
function this:getSettings()
    local test = this:tryConfig()
    if not test then -- 設置不存在
        test = this:newConfig()
    else -- 使用設置
        local ok
        _ = function(...)
            return string.char(...)
        end
        ok, this.Settings = pcall(test)
        _ = nil
        if not ok then
            Toast("Config has reset due to load error")
            this:newConfig()
        end
    end
    return this
end